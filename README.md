# let-me-in

## Application Link
**Deployment:** https://tka3-letmein.herokuapp.com <br>
**Project Repository:** https://gitlab.com/tk-adpro-letmein/let-me-in

## API Link
**Deployment:** https://tka3-letmein-help.herokuapp.com/help/api/question <br>
**Project Repository:** https://gitlab.com/tk-adpro-letmein/help

## Pipeline Status And Coverage Report

**Pipeline** for branch `eventFinal/dion` <br>
**Coverage** consist of  `event`

![pipeline status](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/eventFinal/dion/pipeline.svg)
![coverage report](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/eventFinal/dion/coverage.svg)

<br><br>
**Pipeline** for branch `eventHistoryFinal/jafar` <br>
**Coverage** consist of `eventHistory` and `notification`

![pipeline status](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/eventHistoryFinal/jafar/pipeline.svg)
![coverage report](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/eventHistoryFinal/jafar/coverage.svg)

<br><br>
**Pipeline** for branch `afif/explore` <br>
**Coverage** consist of `explore event` 

![pipeline status](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/afif/explore/pipeline.svg)
![coverage report](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/afif/explore/coverage.svg)

<br><br>
**Pipeline** for branch `helpFinal/novi` <br>
**Coverage** consist of `help-frontend`

![pipeline status](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/helpFinal/novi/pipeline.svg)
![coverage report](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/helpFinal/novi/coverage.svg)

<br><br>
**Pipeline** for branch `authentication/hazim` <br>
**Coverage** consist of `authentication and authorization`

![pipeline status](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/authentication/hazim/pipeline.svg)
![coverage report](https://gitlab.com/tk-adpro-letmein/let-me-in/badges/authentication/hazim/coverage.svg)



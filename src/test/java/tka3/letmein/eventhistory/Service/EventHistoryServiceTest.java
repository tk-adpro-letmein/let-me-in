package tka3.letmein.eventhistory.Service;

import tka3.letmein.eventhistory.model.EventHistory;
import tka3.letmein.eventhistory.repository.EventHistoryRepository;
import tka3.letmein.eventhistory.service.EventHistoryServiceImp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tka3.letmein.event.model.Event;
import tka3.letmein.event.repository.EventRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EventHistoryServiceTest {

    @Mock
    private EventHistoryRepository eventHistoryRepository;

    @Mock
    private EventRepository eventRepository;

    @InjectMocks
    private EventHistoryServiceImp eventHistoryServiceImp;

    private Event event;
    private EventHistory eventHistory;

    @BeforeEach
    public void setUp() {
        event = new Event("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "Pendidikan", "asik banget", 10);
        event.setEventId(1);
        eventHistory = new EventHistory("Jafar Telah di daftarkan");
        eventHistory.setIdLog(1);
        eventHistory.setEventUpdated(event);
        event.getHistoryEvent().add(eventHistory);
    }

    @Test
    public void testGetLogEvent() {
        when(eventRepository.findByEventId(event.getEventId())).thenReturn(event);
        Assertions.assertEquals(eventHistoryServiceImp.getHistoryByEvent(event.getEventId()).size(), event.getHistoryEvent().size());
    }

    @Test
    public void createHistory() {
        when(eventRepository.findByEventId(event.getEventId())).thenReturn(event);
        EventHistory result = eventHistoryServiceImp.createLog(event.getEventId(), "Jafar Telah di daftarkan");
        result.setIdLog(2);
        Assertions.assertEquals(result.getIdLog(), 2);
        Assertions.assertEquals(result.getEventUpdated(), event);
        Assertions.assertEquals(eventHistoryServiceImp.getHistoryByEvent(event.getEventId()).size(), event.getHistoryEvent().size());
        verify(eventHistoryRepository, times(1)).save(any());
    }

}

package tka3.letmein.eventhistory.Controller;

import tka3.letmein.eventhistory.controller.EventHistoryController;
import tka3.letmein.eventhistory.model.EventHistory;
import tka3.letmein.eventhistory.service.EventHistoryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tka3.letmein.event.model.Event;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@WebMvcTest(controllers = EventHistoryController.class)
public class EventHistoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private EventHistoryService eventHistoryServiceImp;

    private Event event;
    private EventHistory eventHistory;


    @BeforeEach
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        event = new Event("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "tes", "asik banget", 10);
        event.setEventId(1);
        eventHistory = new EventHistory("Asik");
        eventHistory.setIdLog(1);
        eventHistory.setEventUpdated(event);
        event.getHistoryEvent().add(eventHistory);
    }

    @Test
    void testGetHistoryEventByIdNotNull() throws Exception {
        Mockito.when(eventHistoryServiceImp.getHistoryByEvent(event.getEventId())).thenReturn(event.getHistoryEvent());
        mvc.perform(get("/history/event/1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("getHistoryEvent"))
            .andExpect(model().attributeExists("History"))
            .andExpect(model().attributeExists("event"))
            .andExpect(view().name("findHistoryByEventId"));
    }

    @Test
    void testGetHistoryEventByIdNull() throws Exception {
        Mockito.when(eventHistoryServiceImp.getHistoryByEvent(4)).thenThrow(new NullPointerException());
        mvc.perform(get("/history/event/4")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("getHistoryEvent"))
            .andExpect(view().name("error"));
    }
}

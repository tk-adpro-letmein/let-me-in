package tka3.letmein.help.controller;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Principal;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = QuestionController.class)
class QuestionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    void whenHelpURLIsAccessedItShouldReturnHelpPage() throws Exception {
        mockMvc.perform(get("/help"))
                .andExpect(status().isOk())
                .andExpect(view().name("help"));
    }

    @Test
    void whenAddQuestionURLIsAccessedItShouldReturnFormQuestionPage() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        when(mockPrincipal.getName()).thenReturn("budi");

        mockMvc.perform(get("/help/add-question")
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("principal"))
                .andExpect(view().name("formQuestion"));
    }

    @Test
    void testControllerAddAnswer() throws Exception{
        Principal mockPrincipal = mock(Principal.class);
        when(mockPrincipal.getName()).thenReturn("budi");

        mockMvc.perform(get("/help/add-answer/1")
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("idQuestion"))
                .andExpect(model().attributeExists("principal"))
                .andExpect(view().name("formAnswer"));
    }

}

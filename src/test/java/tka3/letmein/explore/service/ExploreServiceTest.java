package tka3.letmein.explore.Service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tka3.letmein.event.model.Event;
import tka3.letmein.explore.repository.ExploreRepository;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExploreServiceTest {

    @Mock
    ExploreRepository exploreRepository;

    @InjectMocks
    ExploreServiceImpl exploreServiceImpl;

    private List<Event> listsKeyword;
    private List<Event> lists;

    @BeforeEach
    public void setUp() {
        listsKeyword = new ArrayList<Event>();
        listsKeyword.add(new Event("Event Rapat Satu", "01-01-2021", "12:01",
                "DPR RI", "Government", "Lorem ipsum.", 0));
        listsKeyword.add(new Event("Event Rapat Dua", "01-01-2021", "12:01",
                "DPR RI", "Government", "Lorem ipsum.", 0));

        lists = new ArrayList<Event>(listsKeyword);
        lists.add(new Event("Event Tiga", "01-01-2021", "12:01",
                "FASILKOM UI", "General", "Lorem ipsum.", 0));
        lists.add(new Event("Event Empat", "01-01-2021", "12:01",
                "FASILKOM UI", "General", "Lorem ipsum.", 0));
    }

    @Test
    public void testGetAllEventsEmpty() {
        exploreServiceImpl.getAllEvents();
        Assertions.assertEquals(0, exploreServiceImpl.getAllEvents().size());
    }

    @Test
    public void testGetAllEventsNotEmpty() {
        lenient().when(exploreServiceImpl.getAllEvents()).thenReturn(lists);
        List<Event> allEventService = exploreServiceImpl.getAllEvents();
        Assertions.assertEquals(allEventService, lists);
    }

    @Test
    public void testGetEventsByNoKeyword() {
        exploreServiceImpl.getEventsbyKeyword("Lima");
        Assertions.assertTrue(exploreServiceImpl.getEventsbyKeyword("Lima").isEmpty());
    }


    @Test
    public void testGetEventsByKeyword() {
        when(exploreRepository.findByEventNameContains("Rapat")).thenReturn(listsKeyword);
        Assertions.assertEquals(exploreServiceImpl.getEventsbyKeyword("Rapat"), listsKeyword);
    }
}

package tka3.letmein.explore.Controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tka3.letmein.event.model.Event;
import tka3.letmein.explore.Service.ExploreServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ExploreController.class)
public class ExploreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private ExploreServiceImpl exploreServiceImpl;

    @MockBean
    private List<Event> lists;
    private List<Event> listsKeyword;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        listsKeyword = new ArrayList<Event>();
        listsKeyword.add(new Event("Event Rapat Satu", "01-01-2021", "12:01",
                "DPR RI", "Government", "Lorem ipsum.", 0));
        listsKeyword.add(new Event("Event Rapat Dua", "01-01-2021", "12:01",
                "DPR RI", "Government", "Lorem ipsum.", 0));

        lists = new ArrayList<Event>(listsKeyword);
        lists.add(new Event("Event Tiga", "01-01-2021", "12:01",
                "FASILKOM UI", "General", "Lorem ipsum.", 0));
        lists.add(new Event("Event Empat", "01-01-2021", "12:01",
                "FASILKOM UI", "General", "Lorem ipsum.", 0));

    }

    @Test
    public void getAllEvents() throws Exception {
        when(exploreServiceImpl.getAllEvents()).thenReturn(lists);
        mockMvc.perform(get("/explore/api/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    public void getEventsbyKeyword() throws Exception {
        when(exploreServiceImpl.getEventsbyKeyword("Rapat")).thenReturn(listsKeyword);
        mockMvc.perform(get("/explore/api/Rapat").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].eventName").value(containsString("Rapat")))
                .andExpect(jsonPath("$[1].eventName").value(containsString("Rapat")));
    }
}
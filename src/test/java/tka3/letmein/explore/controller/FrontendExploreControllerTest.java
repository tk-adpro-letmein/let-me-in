package tka3.letmein.explore.Controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.event.model.Event;
import tka3.letmein.explore.Service.ExploreServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ExploreFrontendController.class)
public class FrontendExploreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private ExploreServiceImpl exploreServiceImp;

    private List<Event> listsKeyword;
    private List<Event> lists;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        listsKeyword = new ArrayList<Event>();
        listsKeyword.add(new Event("Event Rapat Satu", "01-01-2021", "12:01",
                "DPR RI", "Government", "Lorem ipsum.", 0));
        listsKeyword.add(new Event("Event Rapat Dua", "01-01-2021", "12:01",
                "DPR RI", "Government", "Lorem ipsum.", 0));

        lists = new ArrayList<Event>(listsKeyword);
        lists.add(new Event("Event Tiga", "01-01-2021", "12:01",
                "FASILKOM UI", "General", "Lorem ipsum.", 0));
        lists.add(new Event("Event Empat", "01-01-2021", "12:01",
                "FASILKOM UI", "General", "Lorem ipsum.", 0));
    }

    @Test
    void testGetAllEvents() throws Exception {
        Mockito.when(exploreServiceImp.getAllEvents()).thenReturn(lists);
        mockMvc.perform(get("/explore")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getAllEvents"))
                .andExpect(model().attributeExists("events"))
                .andExpect(view().name("findEventByKeyword"));
    }

    @Test
    void testGetEventsByKeyword() throws Exception {
        Mockito.when(exploreServiceImp.getEventsbyKeyword("Rapat")).thenReturn(listsKeyword);
        mockMvc.perform(get("/explore")
                .param("keyword", "Rapat")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getAllEvents"))
                .andExpect(model().attributeExists("events"))
                .andExpect(view().name("findEventByKeyword"));

    }
}

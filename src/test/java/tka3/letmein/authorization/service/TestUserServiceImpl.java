package tka3.letmein.authorization.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.RoleRepository;
import tka3.letmein.authorization.repository.UserAccountRepository;
import static org.mockito.Mockito.lenient;


@ExtendWith(MockitoExtension.class)
public class TestUserServiceImpl {

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private UserAccountRepository userAccountRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private UserAccount userAcc;

    @BeforeEach
    public void setUp() throws  Exception{

        userAcc= new UserAccount("mamangkesbor", "mamangkesbor123","mamangkesbor123");


    }
    @Test
    public void testWhenUserIsSaveShouldReturnUserAcc(){
        lenient().when(userService.save(userAcc)).thenReturn(userAcc);
        Assertions.assertEquals(userAcc, userService.save(userAcc));

    }
    @Test
    public void testUserIsSavedInUserRepository(){

        lenient().when(userService.findByUsername("mamangkesbor")).thenReturn(userAcc);
        Assertions.assertEquals(userAcc,userService.findByUsername("mamangkesbor"));
    }

}

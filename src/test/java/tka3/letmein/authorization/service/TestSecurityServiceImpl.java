//package tka3.letmein.authorization.service;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.Spy;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.mockito.Mockito.when;
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration
//public class TestSecurityServiceImpl {
//
//    private MockMvc mockMvc;
//
//    @Mock
//    private AuthenticationManager authenticationManager;
//
//    @Mock
//    private UserDetailsService userDetailsService;
//
//    @Mock
//    public SecurityService securityService;
//
//    @Mock
//    public SecurityContextHolder securityContextHolder;
//
//    @Mock
//    private UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
//
//    @InjectMocks
//    private SecurityServiceImpl securityServiceImpl= new SecurityServiceImpl();
//
//    private static final Logger logger= LoggerFactory.getLogger(SecurityServiceImpl.class);
//
//    @BeforeEach
//    public void setUp(){
//        Authentication authentication = Mockito.mock(Authentication.class);
//        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
//        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
//        SecurityContextHolder.setContext(securityContext);
//
//
//
//    }
//    @Test
//    @WithMockUser
//    public void testIsAuthenticatedWhenUserIsNull () throws Exception{
//        boolean isAuth= securityService.isAuthenticated();
//
//        System.out.println(isAuth);
//
//        Assertions.assertEquals(true, securityService.isAuthenticated());
//
//
//    }
//    }

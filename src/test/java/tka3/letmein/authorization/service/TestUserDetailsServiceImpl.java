package tka3.letmein.authorization.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ReactiveAdapterRegistry;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import tka3.letmein.authorization.models.Role;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.RoleRepository;
import tka3.letmein.authorization.repository.UserAccountRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TestUserDetailsServiceImpl {
    @Mock
    private UserAccountRepository userAccountRepository;


    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    private HashMap<String, UserAccount> userMap;

    private UserAccount userAcc;


    private Set<GrantedAuthority> grantedAuthorities;


    private Set<Role> roles;


    @BeforeEach
    public void setUp(){
        userMap=new HashMap<>();

        Role pengguna= new Role();
        pengguna.setName("ROLE_USER");

        roles=new HashSet<>();
        roles.add(pengguna);

        grantedAuthorities=new HashSet<>();


        userAcc= new UserAccount("user123456","user12345","user12345");
        userAcc.setRoles(roles);

        userMap.put("user123456",userAcc);


    }



    @Test
    public void testLoadUserWhenUserIsNotNullThenReturnUser(){


        when(userAccountRepository.findByUsername("user123456")).thenReturn(userMap.get("user123456"));

        User userDetails= new User(userAcc.getUsername(),userAcc.getPassword(),grantedAuthorities);

        assertEquals(userDetails,userDetailsService.loadUserByUsername(userAcc.getUsername()));

    }
}

//package tka3.letmein.authorization.config;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//
//
//import static org.mockito.Mockito.when;
//import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
//
//
//@SpringBootTest
//public class TestWebSecurityConfig {
//    private MockMvc mockMvc;
//
//    @Mock
//    private UserDetailsService userDetailsService;
//
//    @Autowired
//    private WebApplicationContext webApplicationContext;
//
//    @InjectMocks
//    private WebSecurityConfig webSecurityConfig;
//
//    @BeforeEach
//    public void setup() {
//        this.mockMvc = webAppContextSetup(webApplicationContext).build();
//    }
//
//    @Test
//    void contextLoads() {
//
//        String hashedPassword = "$2y$12$LoUoUX4t54/gDGDNvNf6w.hBhpVffPdoI3lZG0P0bVvEnBRVCsw5i";
//
//        UserDetails user = User.builder()
//                .username("admin")
//                .password(hashedPassword)
//                .roles("ADMIN")
//                .build();
//
//        when(userDetailsService.loadUserByUsername("admin")).thenReturn(user);
//    }
//
//    @Test
//    public void testGoToWelcomeAndExpectedOK() throws Exception{
//        mockMvc.perform(get("/login"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void testGoToRegisterAndExpectedOK() throws Exception{
//        mockMvc.perform(get("/registration"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void testGoToSlashAndSuccessfull() throws Exception{
//        mockMvc.perform(get("/"))
//                .andExpect(status().is2xxSuccessful());
//    }
//
//
//
//
//}

package tka3.letmein.authorization.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.service.SecurityService;
import tka3.letmein.authorization.service.UserService;
import tka3.letmein.authorization.validator.UserValidator;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
@WebMvcTest(UserController.class)
public class TestUserController {
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;
    @MockBean
    private UserValidator userValidator;
    @MockBean
    private SecurityService securityService;
    @InjectMocks
    private UserController userController;
    @Mock
    private BindingResult bindingResult;
//    @Mock
//    private Model model;
//    @Mock
//    private UserAccount userAccount;
    @Autowired
    private WebApplicationContext webApplicationContext;



    @BeforeEach
    public void setup() {
//        mockMvc = MockMvcBuilders
//                .webAppContextSetup(context)
//                .apply(springSecurity())
//                .build();
        this.mockMvc = webAppContextSetup(webApplicationContext).build();


    }

    @Test
    @WithMockUser
    public void whenRegistrationShouldAddModel() throws Exception{
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("registration"))
                .andExpect(model().attributeExists("userForm"))
                .andExpect(view().name("registration"));
        verify(securityService).isAuthenticated();
    }

    @Test
    public void whenRegistrationCompleteShouldRedirectToWelcome() throws  Exception{
        when(securityService.isAuthenticated()).thenReturn(true);
        mockMvc.perform(get("/registration"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }
    @Test
    public void whenRegistrationNotAuthenticatedShouldAddAttr() throws Exception{
        when(securityService.isAuthenticated()).thenReturn(false);

        mockMvc.perform(get("/registration"))
                .andExpect(model().attribute("userForm",new UserAccount()));

    }
    @Test
    public void whenRegistrationNotSuccessStatusShould3xx() throws Exception{
        when(bindingResult.hasErrors()).thenReturn(true);
        mockMvc.perform(post("/registration"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testLoginIsSuccessfull() throws  Exception {
        when(securityService.isAuthenticated()).thenReturn(true);
        mockMvc.perform(get("/login"))
                .andExpect(redirectedUrl("/"));
    }



    }





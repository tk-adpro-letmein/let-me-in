package tka3.letmein.notification.service;

import tka3.letmein.notification.model.MessageNotif;
import tka3.letmein.notification.repository.MessageNotifRepository;

import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.UserAccountRepository;
import tka3.letmein.event.model.Event;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceTest {

    @Mock
    MessageNotifRepository messageNotifRepository;

    @Mock
    UserAccountRepository userAccountRepository;

    @InjectMocks
    NotificationServiceImp notificationServiceImp;

    private Event event;
    private UserAccount user1;
    private MessageNotif messageNotif;

    @BeforeEach
    public void setUp() {
        event = new Event("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 10);
        event.setEventId(0);
        user1 = new UserAccount("iniJafar","password","password");
        user1.setId((long)1234135);
        user1.setMessageUser(new ArrayList<>());
        user1.getEventJoined().add(event);
        event.getPesertaEvent().add(user1);
        messageNotif = new MessageNotif();
        messageNotif.setIdMessage(1);
        messageNotif.setMessage("tes");
        messageNotif.setUserNotif(user1);
        user1.getMessageUser().add(messageNotif);
    }

    @Test
    public void notifUserWhenAnotherUserRegisterEvent() {
        MessageNotif messageNotif = notificationServiceImp.notifMessage(event, user1);
        messageNotif.setIdMessage(2);
        Assertions.assertEquals(messageNotif.getIdMessage(), 2);
        Assertions.assertEquals(messageNotif.getUserNotif(), user1);
        Assertions.assertEquals(user1.getMessageUser().get(1), messageNotif);
        verify(messageNotifRepository, times(1)).save(any());
    }

    @Test
    public void testGetMessageEventNotNull() {
        when(userAccountRepository.findByUsername(user1.getUsername())).thenReturn(user1);
        Assertions.assertEquals(notificationServiceImp.getAllMessage(user1.getUsername()).get(0), messageNotif);
    }
}

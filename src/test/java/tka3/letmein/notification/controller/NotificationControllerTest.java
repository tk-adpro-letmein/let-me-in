package tka3.letmein.notification.controller;

import tka3.letmein.notification.model.MessageNotif;
import tka3.letmein.notification.service.NotificationService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.security.Principal;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.event.model.Event;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = NotificationController.class)
public class NotificationControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private NotificationService notificationServiceImp;

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private Event event;
    private UserAccount user;
    private MessageNotif notifMessage;

    @BeforeEach
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        event = new Event("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 10);
        event.setEventId(0);
        user = new UserAccount("iniJafar","password","password");
        user.setId((long)1234135);
        user.setMessageUser(new ArrayList<>());
        notifMessage = new MessageNotif();
        notifMessage.setUserNotif(user);
        notifMessage.setMessage("tes");
        user.getMessageUser().add(notifMessage);
    }

    @Test
    void notifMessageWhenAnotherUserRegister() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("iniJafar");
        Mockito.when(notificationServiceImp.getAllMessage(mockPrincipal.getName())).thenReturn(user.getMessageUser());
        mvc.perform(get("/messages")
            .principal(mockPrincipal)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("getAllMessage"))
            .andExpect(model().attributeExists("Notif"))
            .andExpect(view().name("notifUser"));
    }

}

package tka3.letmein.event;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.UserAccountRepository;
import tka3.letmein.event.model.Event;
import tka3.letmein.event.repository.EventRepository;
import tka3.letmein.event.service.EventServiceImp;

import java.util.List;
import tka3.letmein.eventhistory.service.EventHistoryServiceImp;
import tka3.letmein.notification.model.MessageNotif;
import tka3.letmein.notification.repository.MessageNotifRepository;
import tka3.letmein.notification.service.NotificationServiceImp;

@ExtendWith(MockitoExtension.class)
class EventServiceImpTest {
    @Mock
    EventRepository eventRepository;
    @Mock
    UserAccountRepository userAccountRepository;
    @Mock
    EventHistoryServiceImp eventHistoryServiceImp;
    @Mock
    NotificationServiceImp notificationServiceImp;
    @InjectMocks
    EventServiceImp eventServiceImp;

    private Event eventOne;
    private Event eventTwo;
    private Event eventThree;
    private UserAccount userAccount;
    private UserAccount userAccountTwo;

    @BeforeEach
    public void setUp(){
        eventOne = new Event ("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 10);
        eventOne.setEventId(1);
        eventTwo = new Event ("Basdat Gaming", "30 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 10);
        eventTwo.setEventId(3);
        eventThree = new Event ("Basdat Gaming", "30 april 2021", "jam 3", "Patek", "iniPoris", "asik banget", 10);
        eventThree.setEventId(7);
        userAccount = new UserAccount("iniJapar","iniPassword","iniPassword");
        userAccountTwo = new UserAccount("iniPoris","iniPassword","iniPassword");
        eventHistoryServiceImp = new EventHistoryServiceImp();
        notificationServiceImp = new NotificationServiceImp();
        eventThree.getPesertaEvent().add(userAccount);
        eventThree.setParticipantsNumber(1);
    }

    @Test
    void testGetAllEventEmpty(){
        eventServiceImp.getAllEvent();
        Assertions.assertEquals(eventServiceImp.getAllEvent().size(), 0 );
    }

    @Test
    void testGetAllEventNotEmpty(){
        List<Event> allEvent = eventRepository.findAll();
        lenient().when(eventServiceImp.getAllEvent()).thenReturn(allEvent);
        List<Event> allEventResult = eventServiceImp.getAllEvent();
        Assertions.assertEquals(allEventResult, allEvent);
    }

    @Test
    void createEvent(){
        lenient().when(eventServiceImp.addEvent(eventOne)).thenReturn(eventOne);
        Event resultEvent = eventServiceImp.addEvent(eventOne);
        Assertions.assertEquals(eventOne.getEventId(), resultEvent.getEventId());
    }

    @Test
    void testGetEventByIdNull(){
        lenient().when(eventRepository.findByEventId(99)).thenThrow(new NullPointerException());
        eventServiceImp.getEvent(99);
        Assertions.assertNull(eventServiceImp.getEvent(99));
    }

    @Test
    void testGetEventByIdNotNull(){
        when(eventRepository.findByEventId(any())).thenReturn(eventOne);
        Assertions.assertEquals(eventServiceImp.getEvent(eventOne.getEventId()), eventOne);
    }

    @Test
    void testDeleteEventByIdComplete(){
        lenient().when(eventRepository.findByEventId(3)).thenReturn(eventTwo);
        eventServiceImp.deleteEvent(3);

        verify(eventRepository, times(1)).deleteById(3);
    }

    @Test
    void testDeleteNotCompleteEventById(){
        lenient().when(eventRepository.findByEventId(1)).thenReturn(eventOne);
        when(userAccountRepository.findByUsername(userAccount.getUsername())).thenReturn(userAccount);
        eventOne = eventServiceImp.registerEvent(eventOne.getEventId(), "iniJapar");

        Assertions.assertEquals("not deleted",eventServiceImp.deleteEvent(1));
    }

    @Test
    void testEditEventWithHistoryLog(){
        lenient().when(eventRepository.findByEventId(eventOne.getEventId())).thenReturn(eventOne);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniJapar", "Gaming sampe gila", 15);
        Event resultEvent = eventServiceImp.editEvent(eventOne.getEventId(), editedEvent, "iniJapar");

        Assertions.assertEquals(editedEvent.getEventName(), resultEvent.getEventName());
        Assertions.assertEquals(editedEvent.getEventDate(), resultEvent.getEventDate());
        Assertions.assertEquals(editedEvent.getEventTime(), resultEvent.getEventTime());
        Assertions.assertEquals(editedEvent.getEventOwner(), resultEvent.getEventOwner());
        Assertions.assertEquals(editedEvent.getEventLocation(), resultEvent.getEventLocation());
        Assertions.assertEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
        Assertions.assertEquals(editedEvent.getEventDescription(), resultEvent.getEventDescription());
    }

    @Test
    void testEditEventWhenParticipantLimitIsOff(){
        lenient().when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniPoris", "Gaming sampe gila", 0);
        Event resultEvent = eventServiceImp.editEvent(eventThree.getEventId(), editedEvent, "iniPoris");

        Assertions.assertNotEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
    }

    @Test
    void testEditEventWhenParticipantLimitIsEqual(){
        lenient().when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniPoris", "Gaming sampe gila", 10);
        Event resultEvent = eventServiceImp.editEvent(eventThree.getEventId(), editedEvent, "iniPoris");

        Assertions.assertEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
    }

    @Test
    void testEditEventWhenParticipantLimitIsEqualAndParticipantNumbersIsNull(){
        lenient().when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);
        eventThree.setParticipantsNumber(null);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniPoris", "Gaming sampe gila", 10);
        Event resultEvent = eventServiceImp.editEvent(eventThree.getEventId(), editedEvent, "iniPoris");

        Assertions.assertEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
    }

    @Test
    void testEditEventWhenParticipantLimitIsNotEqualAndParticipantNumbersIsNull(){
        lenient().when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);
        eventThree.setParticipantsNumber(null);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniPoris", "Gaming sampe gila", 9);
        Event resultEvent = eventServiceImp.editEvent(eventThree.getEventId(), editedEvent, "iniPoris");

        Assertions.assertEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
    }

    @Test
    void testEditEventWhenParticipantLimitIsNotEqualAndOffLimit(){
        lenient().when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniPoris", "Gaming sampe gila", 0);
        Event resultEvent = eventServiceImp.editEvent(eventThree.getEventId(), editedEvent, "iniPoris");

        Assertions.assertNotEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
    }

    @Test
    void testEditEventWhenParticipantNumbersIsNull(){
        lenient().when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);
        eventThree.setParticipantsNumber(null);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniPoris", "Gaming sampe gila", 10);
        Event resultEvent = eventServiceImp.editEvent(eventThree.getEventId(), editedEvent, "iniPoris");

        Assertions.assertEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
    }

    @Test
    void testEditEventWhenNothingIsChangedWithHistoryLog(){
        lenient().when(eventRepository.findByEventId(eventOne.getEventId())).thenReturn(eventOne);
        Event resultEvent = eventServiceImp.editEvent(eventOne.getEventId(), eventOne, "iniJapar");

        Assertions.assertEquals(eventOne.getEventName(), resultEvent.getEventName());
        Assertions.assertEquals(eventOne.getEventDate(), resultEvent.getEventDate());
        Assertions.assertEquals(eventOne.getEventTime(), resultEvent.getEventTime());
        Assertions.assertEquals(eventOne.getEventOwner(), resultEvent.getEventOwner());
        Assertions.assertEquals(eventOne.getEventLocation(), resultEvent.getEventLocation());
        Assertions.assertEquals(eventOne.getParticipantLimit(), resultEvent.getParticipantLimit());
        Assertions.assertEquals(eventOne.getEventDescription(), resultEvent.getEventDescription());
    }

    @Test
    void testEditEventWhenUserIsNotOwner(){
        lenient().when(eventRepository.findByEventId(eventOne.getEventId())).thenReturn(eventOne);

        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniJapar", "Gaming sampe gila", 15);
        Event resultEvent = eventServiceImp.editEvent(eventOne.getEventId(), editedEvent, "iniPoris");

        Assertions.assertNotEquals(editedEvent.getEventName(), resultEvent.getEventName());
        Assertions.assertNotEquals(editedEvent.getEventDate(), resultEvent.getEventDate());
        Assertions.assertNotEquals(editedEvent.getEventTime(), resultEvent.getEventTime());
        Assertions.assertNotEquals(editedEvent.getEventOwner(), "iniPoris");
        Assertions.assertNotEquals(editedEvent.getEventLocation(), resultEvent.getEventLocation());
        Assertions.assertNotEquals(editedEvent.getParticipantLimit(), resultEvent.getParticipantLimit());
        Assertions.assertNotEquals(editedEvent.getEventDescription(), resultEvent.getEventDescription());
    }

    @Test
    void testEditNotExistEvent(){
        lenient().when(eventRepository.findByEventId(99)).thenThrow(new NullPointerException());
        Event editedEvent = new Event("Gaming Statprob", "30 Maret 2021", "jam 9", "Belyos", "iniJapar", "Gaming sampe gila", 15);
        Assertions.assertNull(eventServiceImp.editEvent(99, editedEvent, "iniJapar"));
    }

    @Test
    void testRemoveParticipantEventComplete(){
        lenient().when(eventRepository.findByEventId(1)).thenReturn(eventOne);
        when(userAccountRepository.findByUsername(userAccount.getUsername())).thenReturn(userAccount);
        Event result = eventServiceImp.registerEvent(eventOne.getEventId(), "iniJapar");
        var eventParticipant = eventOne.getParticipantsNumber();
        eventServiceImp.deleteParticipant(result.getEventId(), userAccount.getUsername());

        Assertions.assertEquals(eventParticipant - 1, (int) result.getParticipantsNumber());
    }

    @Test
    void testRegisterEventNotNull(){
        when(eventRepository.findByEventId(eventOne.getEventId())).thenReturn(eventOne);
        when(userAccountRepository.findByUsername("iniPoris")).thenReturn(userAccountTwo);
        Event result = eventServiceImp.registerEvent(eventOne.getEventId(), "iniPoris");
        Assertions.assertEquals(result, eventOne);
    }

    @Test
    void testRegisterEventWhenUserAlreadyIn(){
        when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);
        lenient().when(userAccountRepository.findByUsername("iniJapar")).thenReturn(userAccountTwo);
        eventServiceImp.registerEvent(eventThree.getEventId(),"iniJapar");
        Event result = eventServiceImp.registerEvent(eventThree.getEventId(), "iniJapar");
        Assertions.assertEquals(result, eventThree);
    }

    @Test
    void testRegisterEventNotNullWithBroadcast(){
        when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);
        when(userAccountRepository.findByUsername(userAccountTwo.getUsername())).thenReturn(userAccountTwo);
        eventServiceImp.registerEvent(eventThree.getEventId(), "iniPoris");
        Assertions.assertNotNull(eventThree);
    }

    @Test
    void testRegisterEventWhenEventIsFull(){
        when(eventRepository.findByEventId(eventThree.getEventId())).thenReturn(eventThree);
        when(userAccountRepository.findByUsername(userAccountTwo.getUsername())).thenReturn(userAccountTwo);
        eventThree.setParticipantLimit(1);
        eventServiceImp.registerEvent(eventThree.getEventId(), "iniPoris");
        Assertions.assertNotNull(eventThree);
    }

    @Test
    void testRegisterEventWhenEventNotExist(){
        when(eventRepository.findByEventId(99)).thenThrow(new NullPointerException());
        lenient().when(userAccountRepository.findByUsername(userAccount.getUsername())).thenReturn(userAccount);
        Assertions.assertNull(eventServiceImp.registerEvent(99, "iniJapar"));
    }

    @Test
    void testRegisterEventWhenUserNotExist(){
        when(eventRepository.findByEventId(eventOne.getEventId())).thenReturn(eventOne);
        lenient().when(userAccountRepository.findByUsername("bukanJapar")).thenThrow(new NullPointerException());
        Assertions.assertNull(eventServiceImp.registerEvent(eventOne.getEventId(), "bukanJapar"));
    }
}

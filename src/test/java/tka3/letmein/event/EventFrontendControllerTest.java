package tka3.letmein.event;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.event.model.Event;
import tka3.letmein.event.service.EventServiceImp;
import tka3.letmein.event.controller.EventFrontendController;

import java.security.Principal;
import java.util.ArrayList;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

@WebMvcTest(controllers = EventFrontendController.class)
class EventFrontendControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private EventServiceImp eventServiceImp;

    private Event event;
    private Event newEvent;
    private Event existEvent;
    private Event noParticipantEvent;
    private UserAccount user;

    @BeforeEach
    public void setUp(){
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        event = new Event();
        event.setEventId(0);
        event.setEventName("Mabar Adpro");
        event.setEventDate("30 april 2021");
        event.setEventTime("jam 3");
        event.setEventLocation("Patek");
        event.setEventOwner("iniJapar");
        event.setEventDescription("asik banget");
        event.setParticipantLimit(10);
        event.setHistoryEvent(new ArrayList<>());
        event.setPesertaEvent(new ArrayList<>());
        event.setParticipantsNumber(0);

        newEvent = new Event ("Nugas Basdat", "27 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 50);
        existEvent = new Event ("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 10);
        noParticipantEvent = new Event ("Mabar Adpro", "30 april 2021", "jam 3", "Patek", "iniJapar", "asik banget", 0);
        newEvent.setEventId(1);
        existEvent.setEventId(7);
        noParticipantEvent.setEventId(11);
        user = new UserAccount("iniJafar","password","password");
        user.setId((long)1234135);
        existEvent.getPesertaEvent().add(user);
    }

    @Test
    void whenFindByIdIsAccessedAndExistShouldReturnDesiredEvent() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("iniJafar");
        Mockito.when(eventServiceImp.getEvent(existEvent.getEventId())).thenReturn(existEvent);

        mvc.perform(get("/event/" + existEvent.getEventId())
                .principal(mockPrincipal)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("findEventById"))
                .andExpect(model().attributeExists("event"))
                .andExpect(model().attributeExists("principal"))
                .andExpect(view().name("findEventById"));
    }

    @Test
    void whenFindByIdIsAccessedAndExistShouldReturnDesiredEventButUserIsNotRegistered() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("iniPoris");
        Mockito.when(eventServiceImp.getEvent(existEvent.getEventId())).thenReturn(existEvent);

        mvc.perform(get("/event/" + existEvent.getEventId())
                .principal(mockPrincipal)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("findEventById"))
                .andExpect(model().attributeExists("event"))
                .andExpect(model().attributeExists("principal"))
                .andExpect(view().name("findEventById"));
    }

    @Test
    void whenFindByIdIsAccessedAndNotExistShouldReturn404Page() throws Exception {
        Mockito.when(eventServiceImp.getEvent(99)).thenThrow(new NullPointerException());

        mvc.perform(get("/event/99")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("findEventById"))
                .andExpect(view().name("error"));
    }

    @Test
    void whenEventCreateURLIsAccessedItShouldReturnCreateEventTemplate() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("iniJafar");

        mvc.perform(get("/event/create")
                .principal(mockPrincipal)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("createForm"))
                .andExpect(model().attributeExists("principal"))
                .andExpect(view().name("createEvent"));
    }

    @Test
    void whenEditEventIsAccessedAndExistShoudlReturnEditPage() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(eventServiceImp.getEvent(0)).thenReturn(event);
        Mockito.when(mockPrincipal.getName()).thenReturn("iniJafar");

        mvc.perform(get("/event/" + event.getEventId() + "/edit")
                .principal(mockPrincipal)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("editForm"))
                .andExpect(model().attributeExists("event"))
                .andExpect(view().name("editEvent"));
    }

    @Test
    void whenEditEventIsAccessedAndNotExistShoudlReturn404Page() throws Exception {
        mvc.perform(get("/event/2/edit")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("editForm"))
                .andExpect(view().name("error"));
    }

    @Test
    void whenSearchEventFromSearchBarCompleteShouldRedirectToDesiredEvent() throws Exception {
        mvc.perform(post("/event/getEvent")
                .param("eventId", String.valueOf(event.getEventId()))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(handler().methodName("findEventFromSearchBar"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/event/" + event.getEventId()));
    }

    @Test
    void whenDeleteEventSuccessfulShouldReturnHomePage() throws Exception {
        Mockito.when(eventServiceImp.deleteEvent(0)).thenReturn("deleted");

        mvc.perform(get("/event/"+ event.getEventId() +"/deleteEvent")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("deleteEventById"))
                .andExpect(redirectedUrl("/"));
    }

    @Test
    void whenDeleteEventNotSuccessfulShouldReturnCurrentPage() throws Exception {
        Mockito.when(eventServiceImp.deleteEvent(0)).thenReturn("not deleted");

        mvc.perform(get("/event/"+ event.getEventId() +"/deleteEvent")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("deleteEventById"))
                .andExpect(redirectedUrl("/event/" + event.getEventId()));
    }

    @Test
    void whenDeleteEventFailedShouldReturn404() throws Exception {
        mvc.perform(get("/event/99/deleteEvent")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("deleteEventById"))
                .andExpect(view().name("error"));
    }

    @Test
    void whenCreateEventIsCompletedShouldRedirectToTheCreatedEventPage() throws Exception {
        mvc.perform(post("/event/eventcreated")
                .param("eventName", event.getEventName())
                .param("eventDate", event.getEventDate())
                .param("eventTime", event.getEventTime())
                .param("eventLocation", event.getEventLocation())
                .param("eventOwner", event.getEventOwner())
                .param("eventDescription", event.getEventDescription())
                .param("participantLimit", String.valueOf(event.getParticipantLimit()))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(handler().methodName("eventCreate"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/event/" + event.getEventId()));
    }

    @Test
    void whenCreateEventIsCompletedButParticipantsAreLessThanOneShouldRedirectToTheCreatedEventPage() throws Exception {
        noParticipantEvent.setEventId(0);

        mvc.perform(post("/event/eventcreated")
                .param("eventName", noParticipantEvent.getEventName())
                .param("eventDate", noParticipantEvent.getEventDate())
                .param("eventTime", noParticipantEvent.getEventTime())
                .param("eventLocation", noParticipantEvent.getEventLocation())
                .param("eventOwner", noParticipantEvent.getEventOwner())
                .param("eventDescription", noParticipantEvent.getEventDescription())
                .param("participantLimit", String.valueOf(noParticipantEvent.getParticipantLimit()))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(handler().methodName("eventCreate"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/event/" + noParticipantEvent.getEventId()));
    }

    @Test
    void whenEventEditIsCompletedShouldRedirectToTheCreatedEventPage() throws Exception {
        mvc.perform(post("/event/saveupdates")
                .param("eventId", String.valueOf(event.getEventId()))
                .param("eventName", newEvent.getEventName())
                .param("eventDate", newEvent.getEventDate())
                .param("eventTime", newEvent.getEventTime())
                .param("eventLocation", newEvent.getEventLocation())
                .param("eventOwner", newEvent.getEventOwner())
                .param("eventDescription", newEvent.getEventDescription())
                .param("participantLimit", String.valueOf(newEvent.getParticipantLimit()))
                .param("currentUser", user.getUsername())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(handler().methodName("editEvent"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/event/" + event.getEventId()));
    }

    @Test
    void whenRegisteringToAnEventIsCompletedShouldRedirectToCurrentEventPage() throws Exception {
        mvc.perform(post("/event/register")
                .param("eventId", String.valueOf(event.getEventId()))
                .param("participantName", event.getEventOwner())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(handler().methodName("registerEvent"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/event/" + event.getEventId()));
    }

    @Test
    void whenRemoveParticipantButEventNotExistShoudlReturn404Page() throws Exception {
        Mockito.when(eventServiceImp.getEvent(99)).thenThrow(new NullPointerException());

        mvc.perform(get("/event/99/remove/iniJafar")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("removeParticipant"))
                .andExpect(redirectedUrl(null));
    }

    @Test
    void whenRemoveParticipantAndExistShoudlRedirectPageToCurrentEvent() throws Exception {
        Mockito.when(eventServiceImp.getEvent(event.getEventId())).thenReturn(event);

        mvc.perform(get("/event/"+ event.getEventId() +"/remove/iniJafar")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("removeParticipant"))
                .andExpect(redirectedUrl("/event/" + event.getEventId()));
    }

}

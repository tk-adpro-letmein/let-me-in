package tka3.letmein.notification.repository;

import tka3.letmein.notification.model.MessageNotif;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageNotifRepository extends JpaRepository<MessageNotif, Integer> {
    MessageNotif findByIdMessage(Integer idMessage);
}

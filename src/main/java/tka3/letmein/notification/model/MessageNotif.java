package tka3.letmein.notification.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tka3.letmein.authorization.models.UserAccount;

@Entity
@Table(name = "message_notif")
@Getter
@Setter
@NoArgsConstructor
public class MessageNotif {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idMessage", updatable = false)
    private int idMessage;

    @Column(name = "message")
    private String message;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "userEmail", nullable = false)
    private UserAccount userNotif;
}

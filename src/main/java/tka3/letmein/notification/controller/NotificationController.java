package tka3.letmein.notification.controller;

import tka3.letmein.notification.model.MessageNotif;
import tka3.letmein.notification.service.NotificationService;

import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NotificationController {

    @Autowired
    NotificationService notificationService;

    @GetMapping(path = "/messages")
    public String getAllMessage(Principal principal, Model model) {
        model.addAttribute("Notif", notificationService.getAllMessage(principal.getName()));
        return "notifUser";
    }

}

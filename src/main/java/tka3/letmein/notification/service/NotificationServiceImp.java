package tka3.letmein.notification.service;

import tka3.letmein.notification.model.MessageNotif;
import tka3.letmein.notification.repository.MessageNotifRepository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.UserAccountRepository;
import tka3.letmein.event.model.Event;

@Service
public class NotificationServiceImp implements NotificationService {

    @Autowired
    MessageNotifRepository messageNotifRepository;

    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    public MessageNotif notifMessage(Event event, UserAccount userEvent) {
        int sisaKuota = event.getParticipantLimit() - (event.getParticipantsNumber() + 1);
        String message = "Kuota event " + event.getEventName() + " yang anda ikuti tersisa " + sisaKuota + " orang";
        var messageNotif = new MessageNotif();
        messageNotif.setMessage(message);
        messageNotif.setUserNotif(userEvent);
        messageNotifRepository.save(messageNotif);
        userEvent.getMessageUser().add(messageNotif);
        return messageNotif;
    }

    @Override
    public List<MessageNotif> getAllMessage(String username) {
        var userCheck = userAccountRepository.findByUsername(username);
        return userCheck.getMessageUser();
    }

}

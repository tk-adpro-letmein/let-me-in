package tka3.letmein.notification.service;

import tka3.letmein.notification.model.MessageNotif;

import java.util.List;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.event.model.Event;

public interface NotificationService {

    MessageNotif notifMessage(Event event, UserAccount userEvent);

    List<MessageNotif> getAllMessage(String username);
}

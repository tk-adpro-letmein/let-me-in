package tka3.letmein.explore.repository;

import tka3.letmein.event.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExploreRepository extends JpaRepository<Event, Integer> {
    List<Event> findByEventNameContains(String keyword);
}

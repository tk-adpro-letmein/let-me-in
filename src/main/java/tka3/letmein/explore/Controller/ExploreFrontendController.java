package tka3.letmein.explore.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tka3.letmein.explore.Service.ExploreService;

@Controller
@RequestMapping(path = "/explore")
public class ExploreFrontendController {

    @Autowired
    private ExploreService exploreService;

    @GetMapping(path = "")
    public String getAllEvents(@RequestParam(value = "keyword", required = false) String keyword, Model model) {
        if (keyword == null) {
            model.addAttribute("events", exploreService.getAllEvents());
        } else {
            model.addAttribute("events", exploreService.getEventsbyKeyword(keyword));
        }
        return "findEventByKeyword";
    }
}

package tka3.letmein.explore.Service;

import tka3.letmein.event.model.Event;

import java.util.List;

public interface ExploreService {

    List<Event> getAllEvents();

    List<Event> getEventsbyKeyword(String keyword);
}
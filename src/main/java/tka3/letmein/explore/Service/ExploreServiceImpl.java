package tka3.letmein.explore.Service;

import tka3.letmein.event.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tka3.letmein.explore.repository.ExploreRepository;

import java.util.List;

@Service
public class ExploreServiceImpl implements ExploreService {
    @Autowired
    private ExploreRepository exploreRepository;

    @Override
    public List<Event> getAllEvents() {
        return exploreRepository.findAll();
    }

    @Override
    public List<Event> getEventsbyKeyword(String keyword) {
        return exploreRepository.findByEventNameContains(keyword);
    }
}

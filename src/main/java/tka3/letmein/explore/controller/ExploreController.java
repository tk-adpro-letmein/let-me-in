package tka3.letmein.explore.Controller;

import tka3.letmein.explore.Service.ExploreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/explore")
public class ExploreController {

    @Autowired
    private ExploreService exploreService;

    @GetMapping(path = {"/api/", "/api/{keyword}"}, produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getAllEvents(@PathVariable(required = false) String keyword) {
        if (keyword == null) {
            return ResponseEntity.ok(exploreService.getAllEvents());
        } else {
            return ResponseEntity.ok(exploreService.getEventsbyKeyword(keyword));
        }
    }
}

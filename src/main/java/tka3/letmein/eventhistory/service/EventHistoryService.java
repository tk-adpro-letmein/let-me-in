package tka3.letmein.eventhistory.service;

import tka3.letmein.eventhistory.model.EventHistory;

import java.util.List;

public interface EventHistoryService {

    EventHistory createLog(Integer eventId, String description);

    List<EventHistory> getHistoryByEvent(Integer eventId);
}
package tka3.letmein.eventhistory.service;

import tka3.letmein.eventhistory.model.EventHistory;
import tka3.letmein.eventhistory.repository.EventHistoryRepository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tka3.letmein.event.repository.EventRepository;

@Service
public class EventHistoryServiceImp implements EventHistoryService {

    @Autowired
    private EventHistoryRepository eventHistoryRepository;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public EventHistory createLog(Integer eventId, String description) {
        var checkEvent = eventRepository.findByEventId(eventId);
        EventHistory eventHistory = new EventHistory();
        eventHistory.setDescription(description);
        eventHistory.setEventUpdated(checkEvent);
        eventHistoryRepository.save(eventHistory);
        return eventHistory;
    }

    @Override
    public List<EventHistory> getHistoryByEvent(Integer eventId) {
        var eventFind = eventRepository.findByEventId(eventId);
        return eventFind.getHistoryEvent();
    }
}

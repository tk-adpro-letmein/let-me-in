package tka3.letmein.eventhistory.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tka3.letmein.event.model.Event;

@Entity
@Table(name = "event_history")
@Getter
@Setter
@NoArgsConstructor
public class EventHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "idLog", updatable = false)
    private int idLog;

    @Column(name = "description")
    private String description;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "eventId", nullable = false)
    private Event eventUpdated;

    public EventHistory(String description) {
        this.description = description;
    }
}

package tka3.letmein.eventhistory.repository;

import tka3.letmein.eventhistory.model.EventHistory;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tka3.letmein.event.model.Event;


@Repository
public interface EventHistoryRepository extends JpaRepository<EventHistory, Integer> {

    EventHistory findByIdLog(Integer idLog);

    List<EventHistory> findByEventUpdated(Event event);
}

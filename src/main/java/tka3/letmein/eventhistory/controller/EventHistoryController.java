package tka3.letmein.eventhistory.controller;

import tka3.letmein.eventhistory.service.EventHistoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/history")
public class EventHistoryController {
    @Autowired
    private EventHistoryService eventHistoryService;

    @GetMapping(path = "/event/{id}")
    public String getHistoryEvent(
        @PathVariable("id") Integer id, Model model) {
        try {
            model.addAttribute("History", eventHistoryService.getHistoryByEvent(id));
            model.addAttribute("event", id);
            return "findHistoryByEventId";
        } catch (NullPointerException e) {
            return "error";
        }
    }
}

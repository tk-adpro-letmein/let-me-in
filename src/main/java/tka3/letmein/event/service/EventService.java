package tka3.letmein.event.service;

import tka3.letmein.event.model.Event;

import java.util.List;

public interface EventService {

    List<Event> getAllEvent();

    Event addEvent(Event event);

    Event getEvent(Integer eventId);

    Event editEvent(Integer eventId, Event event, String currentUser);

    String deleteEvent(Integer eventId);

    Event registerEvent(Integer eventId, String user);

    void deleteParticipant(int eventId, String userEmail);

    void broadcast(Event event);
}

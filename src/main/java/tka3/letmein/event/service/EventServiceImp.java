package tka3.letmein.event.service;

import tka3.letmein.event.model.Event;
import tka3.letmein.event.repository.EventRepository;

import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.UserAccountRepository;
import tka3.letmein.eventhistory.service.EventHistoryServiceImp;
import tka3.letmein.notification.service.NotificationServiceImp;


@Service
public class EventServiceImp implements EventService {
    private static final String LOGADDED = "LogAdded";
    Logger logger = Logger.getLogger(EventServiceImp.class.getName());
    private int counter = 0;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private EventHistoryServiceImp eventHistoryServiceImp;

    @Autowired
    private NotificationServiceImp notificationService;

    @Override
    public List<Event> getAllEvent() {
        return eventRepository.findAll();
    }

    @Override
    public Event addEvent(Event event) {
        eventRepository.save(event);
        return event;
    }

    @Override
    public Event getEvent(Integer eventId) {
        try {
            return eventRepository.findByEventId(eventId);
        } catch (NullPointerException e){
            return null;
        }
    }

    @Override
    public Event editEvent(Integer eventId, Event event, String currentUser) {
        try {
            var checkEvent = eventRepository.findByEventId(eventId);
            if (checkEvent.getEventOwner().equals(currentUser)) {
                changeTitle(checkEvent, event.getEventName());
                changeDate(checkEvent, event.getEventDate());
                changeLocation(checkEvent, event.getEventLocation());
                changeDescription(checkEvent, event.getEventDescription());
                changeParticipantLimit(checkEvent, event.getParticipantLimit());
                changeTime(checkEvent, event.getEventTime());

                if (counter > 0) {
                    var deskripsi = "Admin Telah Mengedit Event";
                    eventHistoryServiceImp.createLog(eventId, deskripsi);
                    counter = 0;
                }
            }
            eventRepository.save(checkEvent);
            return checkEvent;
        } catch (NullPointerException e) {
            return null;
        }
    }

    public void changeTitle(Event event, String eventTitle) {
        if (!event.getEventName().equals(eventTitle)) {
            event.setEventName(eventTitle);
            counter++;
        }
    }

    public void changeDate(Event event, String eventDate) {
        if (!event.getEventDate().equals(eventDate)) {
            event.setEventDate(eventDate);
            counter++;
        }
    }

    public void changeLocation(Event event, String eventLocation) {
        if (!event.getEventLocation().equals(eventLocation)) {
            event.setEventLocation(eventLocation);
            counter++;
        }
    }

    public void changeTime(Event event, String eventTime) {
        if (!event.getEventTime().equals(eventTime)) {
            event.setEventTime(eventTime);
            counter++;
        }
    }

    public void changeDescription(Event event, String eventDescription) {
        if (!event.getEventDescription().equals(eventDescription)) {
            event.setEventDescription(eventDescription);
            counter++;
        }
    }

    public void changeParticipantLimit(Event event, Integer eventLimit) {
        if (!event.getParticipantLimit().equals(eventLimit) && (event.getParticipantsNumber() == null || event.getParticipantsNumber() <= eventLimit)) {
            event.setParticipantLimit(eventLimit);
            counter++;
        }
    }

    @Override
    public String deleteEvent(Integer eventId) {
        var checkEvent = eventRepository.findByEventId(eventId);
        if (checkEvent.getPesertaEvent().isEmpty()) {
            eventRepository.deleteById(eventId);
            return "deleted";
        }
        return "not deleted";
    }

    @Override
    public Event registerEvent(Integer eventId, String user) {
        try {
            var checkEvent = eventRepository.findByEventId(eventId);
            var checkUser = userAccountRepository.findByUsername(user);
            if (checkEvent.getParticipantsNumber() < checkEvent.getParticipantLimit()) {
                int index = checkEvent.getPesertaEvent().indexOf(checkUser);
                if (index < 0) {
                    broadcast(checkEvent);
                    checkEvent.getPesertaEvent().add(checkUser);
                    checkUser.getEventJoined().add(checkEvent);
                    checkEvent.setParticipantsNumber(checkEvent.getParticipantsNumber() + 1);
                    userAccountRepository.save(checkUser);
                    eventRepository.save(checkEvent);
                    String deskripsi = user + " Telah ditambahkan";
                    eventHistoryServiceImp.createLog(eventId, deskripsi);
                }
            }
            return checkEvent;
        } catch (NullPointerException e){
            return null;
        }
    }

    @Override
    public void deleteParticipant(int eventId, String userName) {
        var checkEvent = eventRepository.findByEventId(eventId);
        var checkUser = userAccountRepository.findByUsername(userName);
        checkEvent.getPesertaEvent().remove(checkUser);
        checkEvent.setParticipantsNumber(checkEvent.getParticipantsNumber() - 1);
        checkUser.getEventJoined().remove(checkEvent);
        eventRepository.save(checkEvent);
        userAccountRepository.save(checkUser);
        String deskripsi = checkUser.getUsername() + " telah dihapuskan";
        eventHistoryServiceImp.createLog(eventId, deskripsi);
    }

    @Override
    public void broadcast(Event event) {
        if (!event.getPesertaEvent().isEmpty()) {
            for (UserAccount eventMember : event.getPesertaEvent()) {
                notificationService.notifMessage(event, eventMember);
            }
        }
    }

}

package tka3.letmein.event.controller;

import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.event.model.Event;
import tka3.letmein.event.service.EventService;

import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/event")
public class EventFrontendController {
    private static final String REDIRECTEVENT = "redirect:/event/";
    private static final String ERROR = "error";
    private static final String PRINCIPAL = "principal";

    @Autowired
    private EventService eventService;

    @PostMapping(path = "/getEvent")
    public String findEventFromSearchBar(@RequestParam(value = "eventId") Integer eventId) {
        return REDIRECTEVENT + eventId;
    }

    @GetMapping(path = "/{id}")
    public String findEventById(@PathVariable("id") Integer id, Model model, Principal principal) {
        try {
            System.out.println(eventService.getEvent(id));
            var isRegistered = false;
            for(UserAccount toCheck : eventService.getEvent(id).getPesertaEvent()){
                if(toCheck.getUsername().equals(principal.getName())){
                    isRegistered = true;
                }
            }
            model.addAttribute("event", eventService.getEvent(id));
            model.addAttribute(PRINCIPAL, principal.getName());
            model.addAttribute("isRegistered", isRegistered);
            return "findEventById";
        } catch (NullPointerException e){
            return ERROR;
        }
    }


    @GetMapping(path = "/create")
    public String createForm(Model model, Principal principal) {
        model.addAttribute(PRINCIPAL, principal.getName());
        return "createEvent";
    }

    @PostMapping(path = "/eventcreated")
    public String eventCreate(
            @RequestParam(value = "eventName") String eventName,
            @RequestParam(value = "eventDate") String eventDate,
            @RequestParam(value = "eventTime") String eventTime,
            @RequestParam(value = "eventLocation") String eventLocation,
            @RequestParam(value = "eventOwner") String eventOwner,
            @RequestParam(value = "eventDescription") String eventDescription,
            @RequestParam(value = "participantLimit") Integer participantLimit
    ) {
        participantLimit = (participantLimit < 1 ? 1 : participantLimit);
        var toCreate = new Event(eventName, eventDate, eventTime, eventLocation, eventOwner, eventDescription, participantLimit);
        eventService.addEvent(toCreate);
        int id = toCreate.getEventId();
        return REDIRECTEVENT + id;
    }

    @GetMapping(path = "/{id}/edit")
    public String editForm(@PathVariable("id") int eventId, Model model, Principal principal) {
        try {
            model.addAttribute("event", eventService.getEvent(eventId));
            model.addAttribute(PRINCIPAL, principal.getName());
            return "editEvent";
        } catch (NullPointerException e) {
            return ERROR;
        }
    }

    @PostMapping(path = "/saveupdates")
    public String editEvent(
            @RequestParam(value = "eventId") int eventId,
            @RequestParam(value = "eventName") String eventName,
            @RequestParam(value = "eventDate") String eventDate,
            @RequestParam(value = "eventTime") String eventTime,
            @RequestParam(value = "eventLocation") String eventLocation,
            @RequestParam(value = "eventOwner") String eventOwner,
            @RequestParam(value = "eventDescription") String eventDescription,
            @RequestParam(value = "participantLimit") Integer participantLimit,
            @RequestParam(value = "currentUser") String currentUser
    ) {
        var toUpdate = new Event(eventName, eventDate, eventTime, eventLocation, eventOwner, eventDescription, participantLimit);
        eventService.editEvent(eventId, toUpdate, currentUser);
        return REDIRECTEVENT + eventId;
    }

    @GetMapping(path = "{id}/deleteEvent")
    public String deleteEventById(
            @PathVariable("id") int eventId
    ) {
        try {
            String toCheck = eventService.deleteEvent(eventId);
            return (toCheck.equals("deleted") ? "redirect:/" : REDIRECTEVENT + eventId);
        } catch (NullPointerException e) {
            return ERROR;
        }
    }

    @PostMapping(path = "/register")
    public String registerEvent(
            @RequestParam(value = "eventId") Integer eventId,
            @RequestParam(value = "participantName") String participantName) {

        eventService.registerEvent(eventId, participantName);
        return REDIRECTEVENT + eventId;
    }

    @GetMapping(path = "{id}/remove/{userName}")
    public String removeParticipant(
            @PathVariable("id") int eventId,
            @PathVariable("userName") String userName
    ) {
        try {
            eventService.getEvent(eventId);
            eventService.deleteParticipant(eventId, userName);
            return REDIRECTEVENT + eventId;
        } catch (NullPointerException e) {
            return ERROR;
        }
    }
}

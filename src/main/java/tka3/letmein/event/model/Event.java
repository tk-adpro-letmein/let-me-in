package tka3.letmein.event.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.eventhistory.model.EventHistory;

@Entity
@Getter
@Setter
@Table(name = "event")
@NoArgsConstructor
public class Event {

    //Automatically generate ID
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "eventId", updatable = false, nullable = false)
    private int eventId;

    @Column(name = "eventName")
    private String eventName;

    @Column(name = "eventDate")
    private String eventDate;

    @Column(name = "eventTime")
    private String eventTime;

    @Column(name = "eventLocation")
    private String eventLocation;

    @Column(name = "eventOwner")
    private String eventOwner;

    @Column(name = "eventDescription")
    private String eventDescription;

    @Column(name = "participantLimit")
    private Integer participantLimit;

    @Column(name = "participantsNumber")
    private Integer participantsNumber;

    @JsonIgnoreProperties("eventJoined")
    @ManyToMany(mappedBy = "eventJoined")
    private List<UserAccount> pesertaEvent;

    @JsonManagedReference
    @OneToMany(mappedBy = "eventUpdated")
    private List<EventHistory> historyEvent;

    public Event(String eventName, String eventDate, String eventTime, String eventLocation, String eventOwner, String eventDescription, Integer participantLimit) {
        this.eventName = eventName;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventOwner = eventOwner;
        this.eventLocation = eventLocation;
        this.eventDescription = eventDescription;
        this.participantLimit = participantLimit;
        this.historyEvent = new ArrayList<>();
        this.pesertaEvent = new ArrayList<>();
        this.participantsNumber = 0;
    }

}

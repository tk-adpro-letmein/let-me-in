package tka3.letmein.event.repository;

import tka3.letmein.event.model.Event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    Event findByEventId(Integer eventId);
}

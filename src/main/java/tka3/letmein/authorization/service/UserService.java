package tka3.letmein.authorization.service;

import java.util.List;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.event.model.Event;

public interface UserService {
    UserAccount save(UserAccount user);

    UserAccount findByUsername(String username);

}

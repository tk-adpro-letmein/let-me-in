package tka3.letmein.authorization.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tka3.letmein.authorization.models.UserAccount;
import tka3.letmein.authorization.repository.RoleRepository;
import tka3.letmein.authorization.repository.UserAccountRepository;

import java.util.HashSet;
import tka3.letmein.event.model.Event;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserAccountRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserAccount save(UserAccount user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
        return user;

    }
    @Override
    public UserAccount findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}

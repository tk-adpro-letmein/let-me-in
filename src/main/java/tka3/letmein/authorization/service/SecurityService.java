package tka3.letmein.authorization.service;

public interface SecurityService {
    boolean isAuthenticated();
    void autoLogin(String username, String password);
}

package tka3.letmein.authorization.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import tka3.letmein.event.model.Event;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tka3.letmein.notification.model.MessageNotif;

@Entity
@Data
@Table(name="UserAccount")
@NoArgsConstructor
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    @ManyToMany
    private Set<Role> roles;
    @Transient
    private String passwordConfirm;

    @JsonIgnoreProperties("pesertaEvent")
    @ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinTable( name = "event_joined",
            joinColumns = @JoinColumn(name = "userEmail"),
            inverseJoinColumns = @JoinColumn(name = "eventId"))
    private Set<Event> eventJoined;

    @JsonManagedReference
    @OneToMany(mappedBy = "userNotif")
    private List<MessageNotif> messageUser;

    public UserAccount( String username, String password, String passwordConfirm){
        super();
        this.username=username;
        this.password=password;
        this.passwordConfirm=passwordConfirm;
        this.eventJoined = new HashSet<>();

    }
}

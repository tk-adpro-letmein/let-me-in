package tka3.letmein.authorization.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tka3.letmein.authorization.models.Role;

public interface RoleRepository extends JpaRepository<Role,Long> {
}

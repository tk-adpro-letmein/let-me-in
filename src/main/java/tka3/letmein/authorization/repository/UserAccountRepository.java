package tka3.letmein.authorization.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import tka3.letmein.authorization.models.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
    UserAccount findByUsername(String username);
}

package tka3.letmein.help.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@Controller
@RequestMapping(path = "/help")
public class QuestionController {

    @GetMapping(path = "")
    public String helpAsync(){
        return "help";
    }

    @GetMapping(path = "/add-question")
    public String addAsync(Model model, Principal principal){
        model.addAttribute("principal", principal.getName());
        return "formQuestion";
    }

    @GetMapping(path = "/add-answer/{idParent}")
    public String addAnswerAsync(Model model, Principal principal, @PathVariable(value = "idParent") int idParent){
        model.addAttribute("idQuestion", idParent);
        model.addAttribute("principal", principal.getName());
        return "formAnswer";
    }

}
